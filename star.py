import math
import distances
def true_sizeclass(small_m:float,r:float)->float:
    return 5 * math.log(r/(10*distances.parsec)) - small_m
def apparent_sizeclass(capital_m:float,r:float) -> float:
    return 5 * math.log(r/(10*distances.parsec)) + capital_m
def distance_of_a_star(small_m: float,capital_m:float) -> float:
    return (10**5*distances.parsec*10**(small_m-capital_m))**(1/5)
#distance of nearby stars
def tanpi_cosmology(capital_r:float,small_r:float):
    return capital_r / small_r
def distance_of_star_pc(capital_r:float,small_r:float) -> float:
    return capital_r / tanpi_cosmology(capital_r,small_r)
def radius_of_earths_orbit(capital_r:float,small_r:float) -> float:
    return tanpi_cosmology(capital_r,small_r) * small_r
#star or galaxys distancing speed
def distancing_velocity(lambd_one:float,lambd_zero:float) -> float:
    return distances.ligthsecond_m * (lambd_one-lambd_zero)/lambd_zero
def perceived_wavelength(lambd_zero:float,velocity:float) -> float:
    return (-distances.ligthsecond_m*lambd_zero+lambd_zero*velocity)/distances.ligthsecond_m