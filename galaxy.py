#distances for a galaxy
def escape_velocity_of_a_galaxy(h: float,r:float) -> float:
    return h*r
def distance_of_a_galaxy(v:float,h:float) -> float:
    return v/h
def hubble_parameter_approx(v:float,r:float) -> float:
    return v/r
