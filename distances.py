import timeunits
astronomical_unit_m = 1.49597870*10**11
parsec = astronomical_unit_m * 206265
lightyear_m = parsec * 0.3066
ligthsecond_m = lightyear_m / (timeunits.julian_year_d*timeunits.midsunday_st_s)
lightminute_m = ligthsecond_m * 60
lighthour_m = lightminute_m * 60
lightday_m = lighthour_m * 24